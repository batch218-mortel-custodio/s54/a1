import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard.js';

export default function Courses(){
	console.log(coursesData);
	// so that you dont need to write <CourseCard> multiple times here...
	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} course={course}/>
		)
	})

	// then render the variable here that contains the CourseCard contents, regardless if thousands of courses Exist
	return(
		<>
		{courses}
		</>
	)
}