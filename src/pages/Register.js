
import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';


import {Navigate} from 'react-router-dom';
import UserContext from "../UserContext";

export default function Register() {

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);

	const {user} = useContext(UserContext);


	// if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	//setIsActive(true)};
	//else{
		//setIsActive(false);
	//}

	console.log(email);
	console.log(password1);
	console.log(password2);


	function registerUser(e){
		e.preventDefault()

		setEmail("");
		setPassword1("");
		setPassword2("");

		alert("Thank you for registering!");
	}


	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

		// if (email !== ""){
		// 	if (password1 !== "" && password2 !== ""){
		// 		if (password1 === password2){
		// 			document.querySelector("#submitBtn").removeAttribute('disabled');
		// 		}
		// 		else{
		// 			document.querySelector("#submitBtn").setAttribute('disabled', true);
		// 		}
		// 	}
		// }
	}, [email, password1, password2])

    return (
    	(user.email !== null) ?
    	<Navigate to="/courses" />
    	:
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" value={email} 
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" value={password1}
	                placeholder="Password"
	                onChange={e => setPassword1(e.target.value)} 
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" value={password2}
	                onChange={e => setPassword2(e.target.value)}  
	                required
                />
            </Form.Group>
            	{/*// ternary operator*/}
            	{ isActive ? 

            	<Button variant="primary" type="submit" id="submitBtn">
            		Submit
            	</Button>
            	:
            	<Button variant="danger" type="submit" id="submitBtn" disabled>
            		Submit
            	</Button>     	
            	}


        </Form>
    )

}