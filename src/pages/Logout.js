
import {Navigate} from 'react-router-dom';

import UserContext from "../UserContext";

import {useEffect, useContext} from 'react';


export default function Logout() {
	// consume the UserContext  object and destructure it to accesss the user state and unsetUser function from context provider.
	const {unsetUser, setUser} = useContext(UserContext);

		//localStorage.clear();
	// clear the localStorage of the user's info
	unsetUser();

	useEffect(() =>{
		setUser({email: null});
	})

	return (

		<Navigate to="/login" />
	)


}