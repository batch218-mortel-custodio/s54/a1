import './App.css';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import { useState } from 'react';

import {UserProvider} from "./UserContext";



import {Container} from 'react-bootstrap';

// Single Page Application (like facebook)
function App() {

  // State hook for the user state (Global Scope)

  // This will be used to store the user info and will be used for validating if a user is logged in on app or not
  const [user, setUser]  = useState({email: localStorage.getItem('email')});

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
            <Route path="*" element={<Error />}/>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
          </Routes>
      </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
